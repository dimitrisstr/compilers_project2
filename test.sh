#!/bin/bash
test_dir="test_files/minijava-error-extra"
test_files=`ls ${test_dir}/*.java`
class_dir="bin"
executable="Main"
numberOfFiles=0
start=`date +%s.%N`

for file in ${test_files};
do
    let numberOfFiles++
    java -cp ${class_dir} ${executable} ${file}
done

end=`date +%s.%N`
runtime=$(echo "$end - $start" | bc)
echo 
echo "Time: ${runtime}"
echo "Number of files: ${numberOfFiles}"

