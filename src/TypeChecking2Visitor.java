import syntaxtree.*;
import syntaxtree.Expression;
import visitor.*;

public class TypeChecking2Visitor extends GJDepthFirst<String, String>
{

    int errors = 0;

    public int getErrors()
    {
        return errors;
    }

    public String visit(MainClass n, String argu) throws Exception
    {
        Workspace.currentClass = n.f1.accept(this, null);
        Workspace.currentMethod = "main";
        //var declarations
        n.f14.accept(this, null);

        //statements
        n.f15.accept(this, null);
        return null;
    }

    public String visit(ClassDeclaration n, String argu) throws Exception
    {
        Workspace.currentClass = n.f1.accept(this, null);
        // var declarations
        n.f3.accept(this, null);

        //method declarations
        n.f4.accept(this, null);

        return null;
    }

    public String visit(ClassExtendsDeclaration n, String argu) throws Exception
    {
        Workspace.currentClass = n.f1.accept(this, null);

        //var declarations
        n.f5.accept(this, null);

        //method declarations
        n.f6.accept(this, null);

        return null;
    }

    public String visit(VarDeclaration n, String argu) throws Exception
    {
        String type = n.f0.accept(this, null);
        if(type.equals("int") || type.equals("boolean") || type.equals("int[]") )
            return type;
        else
        {
            if (Workspace.getClass(type) == null)
                throw new Exception("\nLine: " + n.f2.beginLine + " error: Cannot find symbol\n" +
                              "symbol:    class " + type);
            else
                return type;
        }
    }

    public String visit(MethodDeclaration n, String argu) throws Exception
    {
        Workspace.currentMethod = n.f2.accept(this, null);
        //variable declarations
        n.f7.accept(this, null);
        //statements
        n.f8.accept(this, null);

        String returnType = n.f1.accept(this, "class");
        String returnExpr = n.f10.accept(this, "type");

        if(returnType.equals(returnExpr) || Workspace.isSuperType(returnExpr, returnType))
            return returnType;
        else
            throw new Exception("\nLine: " + n.f11.beginLine +
                    " error: Incompatible types: " + returnExpr + " cannot be converted to " + returnType);
    }


    /*
        Statements
     */

    public String visit(Statement n, String argu) throws Exception
    {
        try
        {
            n.f0.choice.accept(this, argu);
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            errors++;
        }
        return null;
    }

    public String visit(PrintStatement n, String argu) throws Exception
    {
        //expression
        String type = n.f2.accept(this, "type");
        if(!type.equals("int"))
            throw new Exception("\nLine: " + n.f0.beginLine + " error: Incompatible type\n" +
                    "Expression's type:  " + type + "\nExpected type:    int");
        return null;
    }

    public String visit(IfStatement n, String argu) throws Exception
    {
        try
        {
            String exprType = n.f2.accept(this, "type");
            if (!exprType.equals("boolean"))
            {
                errors++;
                System.out.println("\nLine: " + n.f1.beginLine +
                        " error: incompatible types: " + exprType + " cannot be converted to boolean");
            }
        }
        catch (Exception e)
        {
            errors++;
            System.out.println(e.getMessage());
        }

        //statements
        n.f4.accept(this, null);
        n.f6.accept(this, null);
        return null;
    }

    public String visit(WhileStatement n, String argu) throws Exception
    {
        try
        {
            String exprType = n.f2.accept(this, "type");
            if (!exprType.equals("boolean"))
            {
                errors++;
                System.out.println("\nLine: " + n.f1.beginLine +
                        " error: incompatible types: " + exprType + " cannot be converted to boolean");
            }
        }
        catch(Exception e)
        {
            errors++;
            System.out.println(e.getMessage());
        }
        //statement
        n.f4.accept(this, null);

        return null;
    }

    public String visit(AssignmentStatement n, String argu) throws  Exception
    {
        String identifierType = n.f0.accept(this, "type");
        String exprType = n.f2.accept(this, "type");


        //identifier's type = expression's type || identifier's type is superClass of expression's type
        if(identifierType.equals(exprType) || Workspace.isSuperType(exprType, identifierType))
            return exprType;
        else
            throw new Exception("\nLine: " + n.f1.beginLine +
                                 "  error: incompatible types: " + exprType + " cannot be converted to "
                                 + identifierType);
    }

    public String visit(ArrayAssignmentStatement n, String argu) throws Exception
    {
        String identifierType = n.f0.accept(this, "type");

        try
        {
            if (!identifierType.equals("int[]"))
                throw new Exception("\nLine: " + n.f1.beginLine + " error: array required, but " +
                        identifierType + " found");
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            errors++;
        }

        String expr1 = n.f2.accept(this, "type");
        if(expr1.equals("int"))
        {
            String expr2 = n.f5.accept(this, "type");
            if(!expr2.equals("int"))
                throw new Exception("\nLine: " + n.f1.beginLine + "  error: incompatible types: "
                        + expr2 + " cannot be converted to int");
        }
        else
            throw new Exception("\nLine: " + n.f1.beginLine + "  error: incompatible types: "
                    + expr1 + " cannot be converted to int");

        return null;
    }


    /*
        Expressions
     */

    public String visit(ArrayLookup n, String argu) throws Exception
    {
        String primeExpr1 = n.f0.accept(this, "type");

        if(primeExpr1.equals("int[]"))
        {
            String expr2 = n.f2.accept(this, "type");
            if(expr2.equals("int"))
                return "int";
            else
                throw new Exception("\nLine: " + n.f1.beginLine + " error: incompatible types: "
                        + expr2 + " cannot be converted to int");
        }
        else
            throw new Exception("\nLine: " + n.f1.beginLine + " error: array required, but " +
                                primeExpr1 + " found");
    }

    public String visit(ArrayLength n, String argu) throws Exception
    {
        String type = n.f0.accept(this, "type");
        if(type.equals("int[]"))
            return "int";
        else
            throw new Exception("\nLine: " + n.f1.beginLine + " error: array required, but " +
                    type + " found");
    }

    public String visit(ArrayAllocationExpression n, String argu) throws Exception
    {
        String expr = n.f3.accept(this, "type");
        if(!expr.equals("int"))
            throw new Exception("\nLine: " + n.f1.beginLine +
                    " error: incompatible types: " + expr + " cannot be converted to int");
        else
            return "int[]";
    }

    public String visit(AllocationExpression n, String argu) throws Exception
    {
        String identifier = n.f1.accept(this, null);
        if(Workspace.getClass(identifier) == null)
            throw new Exception("\nLine: " + n.f0.beginLine + " error: " +
                " cannot find symbol\nsymbol: class " + identifier +
                    "\nlocation: class " + Workspace.currentClass);
        else
            return identifier;
    }


    public String visit(MessageSend n, String argu) throws Exception
    {
        String classID = n.f0.accept(this, "type");

        String methodName = n.f2.accept(this, null);
        String method = Workspace.getMethodType(methodName, classID);
        if(method == null)
            throw new Exception("\nLine: " + n.f1.beginLine + " error: " +
                    "cannot find symbol\nsymbol: method " + methodName);

        String exprList = "";
        if(n.f4.present())
            exprList = n.f4.accept(this, "type");
        Workspace.checkMethodArgs(exprList, classID, methodName, n.f1.beginLine);
        return method;
    }

    public String visit(NodeListOptional n, String argu) throws  Exception
    {
        String result = "";
        for(int i = 0; i < n.size(); i++)
        {
            try
            {
                result += n.elementAt(i).accept(this, argu) + " ";
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());
                errors++;
            }
        }
        return result;
    }

    public String visit(ExpressionList n, String argu) throws Exception
    {
        String parTypes = "";
        parTypes += n.f0.accept(this, "type") + " ";
        parTypes += n.f1.accept(this, "type") + " ";
        return parTypes;
    }

    public String visit(ExpressionTerm n, String argu) throws Exception
    {
        return n.f1.accept(this, argu);
    }

    public String visit(ExpressionTail n, String argu) throws Exception
    {
        return n.f0.accept(this, argu);
    }

    public String visit(Expression n, String argu) throws Exception
    {
        return n.f0.accept(this, argu);
    }

    public String visit(PrimaryExpression n, String argu) throws Exception
    {
        return n.f0.accept(this, argu);
    }

    public String visit(NotExpression n, String argu) throws Exception
    {
        return n.f1.accept(this, "type");
    }

    public String visit(BracketExpression n, String argu) throws Exception
    {
        return n.f1.accept(this, "type");
    }

    public String visit(CompareExpression n, String argu) throws Exception
    {
        String type1 = n.f0.accept(this, "type");
        String type2 = n.f2.accept(this, "type");

        if(type1.equals("int") && type2.equals("int"))
            return "boolean";
        else
            throw new Exception("\nLine: " + n.f1.beginLine +
                                "  error: bad operand types for binary operator '<' \n" +
                                "first type: " + type1 + "\nsecond type: " + type2);

    }

    public String visit(AndExpression n, String argu) throws Exception
    {
        String type1 = n.f0.accept(this, "type");
        String type2 = n.f2.accept(this, "type");

        if(type1.equals("boolean") && type2.equals("boolean"))
            return "boolean";
        else
            throw new Exception("\nLine: " + n.f1.beginLine +
                                "  error: bad operand types for binary operator '&&' \n" +
                                "first type: " + type1 + "\nsecond type: " + type2);
    }

    public String visit(PlusExpression n, String argu) throws Exception
    {
        String type1 = n.f0.accept(this, "type");
        String type2 = n.f2.accept(this, "type");
        if(type1.equals("int") && type2.equals("int"))
            return "int";
        else
            throw new Exception("\nLine: " + n.f1.beginLine +
                                "  error: bad operand types for binary operator '+' \n" +
                                "first type: " + type1 + "\nsecond type: " + type2);
    }

    public String visit(MinusExpression n, String argu) throws Exception
    {
        String type1 = n.f0.accept(this, "type");
        String type2 = n.f2.accept(this, "type");

        if(type1.equals("int") && type2.equals("int"))
            return "int";
        else
            throw new Exception("\nLine: " + n.f1.beginLine +
                                "  error: bad operand types for binary operator '-' \n" +
                                "first type: " + type1 + "\nsecond type: " + type2);
    }

    public String visit(TimesExpression n, String argu) throws Exception
    {
        String type1 = n.f0.accept(this, "type");
        String type2 = n.f2.accept(this, "type");

        if(type1.equals("int") && type2.equals("int"))
            return "int";
        else
            throw new Exception("\nLine: " + n.f1.beginLine +
                                "  error: bad operand types for binary operator '*' \n" +
                                "first type: " + type1 + "\nsecond type: " + type2);
    }

    public String visit(Clause n, String argu) throws Exception
    {
        return n.f0.accept(this, argu);
    }

    public String visit(NodeChoice n, String argu) throws Exception
    {
        return n.choice.accept(this, argu);
    }

    /*
        Types
     */

    public String visit(Type n, String argu) throws Exception
    {
        return n.f0.choice.accept(this, argu);
    }

    public String visit(ArrayType n, String argu) throws Exception
    {
        return "int[]";
    }

    public String visit(BooleanType n, String argu) throws Exception
    {
        return n.f0.toString();
    }

    public String visit(IntegerType n, String argu) throws Exception
    {
        return n.f0.toString();
    }

    /*
        Literals
     */

    public String visit(IntegerLiteral n, String argu) throws Exception
    {
        return "int";
    }

    public String visit(TrueLiteral n, String argu) throws Exception
    {
        return "boolean";
    }

    public String visit(FalseLiteral n, String argu) throws Exception
    {
        return "boolean";
    }

    public String visit(Identifier n, String argu) throws Exception
    {
        if(argu == "type")
        {
            String type = Workspace.findIdentifier(n.f0.toString());
            if(type == null)
                throw new Exception("\nLine: " + n.f0.beginLine + " error: " +
                        "cannot find symbol\n" +
                        "symbol: variable " + n.f0.toString());
            else
                return type;
        }
        else if(argu == "class")
        {
            String classType = n.f0.toString();
            if(Workspace.getClass(classType) == null)
                throw new Exception("\nLine: " + n.f0.beginLine + " error: " +
                        "cannot find symbol\n" +
                        "symbol: class " + classType);
            else
                return  classType;
        }
        else
            return n.f0.toString();
    }

    public String visit(ThisExpression n, String argu) throws Exception
    {
        return Workspace.currentClass.toString();
    }

}
