import syntaxtree.*;
import java.io.*;
import java.util.Map;


class Main {
    public static void main (String [] args) throws Exception
    {
	    if(args.length == 0){
	        System.err.println("Usage: java Driver <inputFile>");
	        System.exit(1);
	}
	FileInputStream fis = null;
	try
	{
		for(String str : args)
        {
            fis = new FileInputStream(str);
            MiniJavaParser parser = new MiniJavaParser(fis);
            System.out.println("\n---------------------------------------------------");
            System.out.println("Input program:  " + str);
            System.out.println("---------------------------------------------------");
            System.err.println(" -> Program parsed successfully.");

            TypeChecking1Visitor check1 = new TypeChecking1Visitor();
            TypeChecking2Visitor check2 = new TypeChecking2Visitor();

            Goal root = parser.Goal();


            System.out.println("Type check 1/2...");
            root.accept(check1, null);
            if(check1.getErrors() > 0)
            {
                System.out.println("\nPhase 1 errors: " + check1.getErrors() + "\nPhase 2 canceled");
                continue;
            }
            System.out.println("Phase 1 completed successfully.");

            System.out.println("Type check 2/2...");
            root.accept(check2, null);
            if(check2.getErrors() > 0)
            {
                System.out.println("\nPhase 2 errors: " + check2.getErrors());
                continue;
            }
            System.out.println("Phase 2 completed successfully.");

            System.out.println(" -> Type checking completed. No errors were found.");

            Workspace.clear();
        }
/*
        for(Map.Entry<String, ClassN> entry : Workspace.classTable.entrySet())
        {
            System.out.println("Class " + entry.getKey());
            entry.getValue().printFields();
            entry.getValue().printMethods();
            System.out.println("--------------------");
        }

		for(Map.Entry<String, String> entry : Workspace.superTypes.entrySet())
        {
            System.out.println("Subclass " + entry.getKey() + " -> SuperClass " + entry.getValue());
        }
*/
	}
	catch(ParseException ex)
    {
	    System.out.println(ex.getMessage());
	}
	catch(FileNotFoundException ex)
    {
	    System.err.println(ex.getMessage());
	}
	finally
    {
	    try
        {
		    if(fis != null) fis.close();
	    }
	    catch(IOException ex)
        {
		    System.err.println(ex.getMessage());
	    }
	}
    }
}
