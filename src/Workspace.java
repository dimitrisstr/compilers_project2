import java.util.*;

class Workspace
{
    public static String currentClass;
    public static String currentMethod;

    //map<identifier, class> classTable;
    public static Map<String, ClassN> classTable = new LinkedHashMap<>();
    //map<subclass, superclass> superTypes;
    public static Map<String, String> superTypes = new LinkedHashMap<>();

    public static int insertClass(String identifier, ClassN classn)
    {
        if(classTable.containsKey(identifier))
        {
            return 0;
        }
        else
        {
            classTable.put(identifier, classn);
            return 1;
        }
    }

    public static int insertSuperClass(String subclass, String superclass)
    {
        if(superTypes.containsKey(subclass))
        {
            return 0;
        }
        else
        {
            superTypes.put(subclass, superclass);
            return 1;
        }
    }

    public static ClassN getClass(String identifier)
    {
        if(classTable.containsKey(identifier))
            return classTable.get(identifier);
        else
            return null;
    }

    public static boolean isSuperType(String subclass, String superclass)
    {
        if(subclass.equals(superclass))
            return true;
        else if(superTypes.containsKey(subclass))
            return isSuperType(superTypes.get(subclass), superclass);
        else
            return false;
    }

    public static void checkMethodArgs(String args, String Class, String method, int line) throws Exception
    {
        Method m = findMethod(Class, method);
        String pars = m.returnArgs();
        int args1Length, args2Length;
        
        String[] args1 = args.split(" ");
        String[] args2 = pars.split(" ");

        if(args.isEmpty())
            args1Length = 0;
        else
            args1Length = args1.length;

        if(pars.isEmpty())
            args2Length = 0;
        else
            args2Length = args2.length;

        if(args1Length != args2Length)
            throw new Exception("\nLine: " + line + " error: Wrong number of arguments for method "
                    + method + "\nArguments found: " + args1Length + "\nArguments expected: " + args2Length );

        for(int i = 0; i < args1Length; i++)
        {
            if(!(args1[i].equals(args2[i]) || isSuperType(args1[i], args2[i])))
                throw new Exception("\nLine: " + line + " error: Incompatible types: " +
                    args1[i] + " cannot be converted to " + args2[i]);
        }
    }

    public static Method findMethod(String className, String methodName)
    {
        ClassN cl = classTable.get(className);
        Method method = cl.getMethod(methodName);

        if(method == null)
        {
            if(superTypes.containsKey(className))
            {
                return findMethod(superTypes.get(className), methodName);
            }
            else
            {
                return null;
            }
        }
        else
        {
            return method;
        }
    }

    public static String getMethodType(String identifier, String className)
    {
        ClassN cl;
        if(className == null)
            cl = getClass(currentClass);
        else
            cl = getClass(className);

        Method method = cl.getMethod(identifier);

        if(method == null)
        {
            if(superTypes.containsKey(className))
            {
                return getMethodType(identifier, superTypes.get(className));
            }
            else
            {
                return null;
            }
        }
        else
            return method.getReturnType();
    }

    public static String findIdentifier(String identifier)
    {
        ClassN cl = getClass(currentClass);
        Method method = cl.getMethod(currentMethod);
        String type;
        if((type = method.getVarType(identifier)) != null)
            return type;
        else if ((type = method.getParType(identifier)) != null)
            return type;
        else if ((type = cl.getField(identifier)) != null)
            return type;
        else
        {
            String subClass = currentClass;
            while(superTypes.containsKey(subClass))
            {
                cl = getClass(superTypes.get(subClass));
                type = cl.getField(identifier);
                if(type != null)
                    return type;
                subClass = superTypes.get(subClass);
            }
            return null;
        }
    }

    public static void clear()
    {
        currentClass = null;
        currentMethod = null;
        classTable.clear();
        superTypes.clear();
    }

}

class Method
{
    //map<identifier, type> varTable;
    private Map<String, String> varTable;
    //map<identifier, type> parametersTable;
    private Map<String, String> parametersTable;
    private String returnType;

    public Method(String returnType)
    {
        this.returnType = returnType;
        varTable = new LinkedHashMap<>();
        parametersTable = new LinkedHashMap<>();
    }

    public String returnArgs()
    {
        String res = "";
        for(Map.Entry<String, String> entry : parametersTable.entrySet())
        {
            res += entry.getValue() + " ";
        }
        return res;
    }


    public int insertParameter(String identifier, String type)
    {
        if(parametersTable.containsKey(identifier))
        {
            return 0;
        }
        else
        {
            parametersTable.put(identifier, type);
            return 1;
        }
    }

    public int insertVariable(String identifier, String type)
    {
        //return 0 (error) if var is already declared or there is a parameter with the same name
        if(varTable.containsKey(identifier) || parametersTable.containsKey(identifier))
        {
            return 0;
        }
        else
        {
            varTable.put(identifier, type);
            return 1;
        }
    }

    public String getVarType(String identifier)
    {
        if(varTable.containsKey(identifier))
            return varTable.get(identifier);
        else
            return null;
    }

    public String getParType(String identifier)
    {
        if(parametersTable.containsKey(identifier))
            return parametersTable.get(identifier);
        else
            return null;
    }

    public String getReturnType()
    {
        return this.returnType;
    }



    public void printVars()
    {
        for(Map.Entry<String, String> entry : varTable.entrySet())
        {
            System.out.println(entry.getValue() + " " + entry.getKey() + ";");
            System.out.print("\t");
        }
        System.out.println();
    }

    public void printParameters()
    {
        for(Map.Entry<String, String> entry : parametersTable.entrySet())
        {
            System.out.print(entry.getValue() + " " + entry.getKey() + ", ");
        }
    }
}

class ClassN
{
    //Map<identifier, Method object> methodTable
    private Map<String, Method> methodTable;
    //map<identifier, type> fieldTable;
    private Map<String, String> fieldTable;

    public ClassN()
    {
        methodTable = new LinkedHashMap<>();
        fieldTable = new LinkedHashMap<>();
    }

    public int insertMethod(String identifier, Method method)
    {
        if(methodTable.containsKey(identifier))
        {
            return 0;
        }
        else
        {
            methodTable.put(identifier, method);
            return 1;
        }
    }

    public int insertField(String identifier, String type)
    {
        if(fieldTable.containsKey(identifier))
        {
            return 0;
        }
        else
        {
            fieldTable.put(identifier, type);
            return 1;
        }
    }

    public Method getMethod(String identifier)
    {
        if(methodTable.containsKey(identifier))
            return methodTable.get(identifier);
        else
            return null;
    }

    public String getField(String identifier)
    {
        //return field's type
        if(fieldTable.containsKey(identifier))
            return fieldTable.get(identifier);
        else
            return null;
    }


    public void printMethods()
    {
        for(Map.Entry<String, Method> entry : methodTable.entrySet())
        {
            System.out.print(entry.getValue().getReturnType() + " " + entry.getKey() + "(");
            entry.getValue().printParameters();
            System.out.println(")\n{");
            System.out.print("\t");
            entry.getValue().printVars();
            System.out.println("}");
        }
    }

    public void printFields()
    {
        System.out.print("\n\t");
        for(Map.Entry entry : fieldTable.entrySet())
        {
            System.out.println(entry.getValue() + " " + entry.getKey() + ";");
            System.out.print("\t");
        }
        System.out.println();
    }


}
