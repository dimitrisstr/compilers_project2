import syntaxtree.*;
import visitor.*;

public class TypeChecking1Visitor extends GJDepthFirst<String, String>
{

    int errors = 0;

    public int getErrors()
    {
        return errors;
    }

     /*
        Class declarations
     */

    public String visit(MainClass n, String argu) throws Exception {

        //Add Main Class
        ClassN newClass = new ClassN();
        Method newMethod = new Method("void");

        //find var declarations
        if (n.f14.present())
        {
            //str = (type identifier line )*
            String str = n.f14.accept(this, null);
            String[] declarations = str.split(" ");
            //for every declared variable
            for (int i = 0; i < declarations.length; i += 3)
            {
                //variable declaration exists
                if (newMethod.insertVariable(declarations[i + 1], declarations[i]) == 0)
                {
                    errors++;
                    System.out.println("\nLine: " + declarations[i + 2] + " error: variable "
                            + declarations[i + 1] + " is already declared in method main");
                }
            }
        }
        newClass.insertMethod("main", newMethod);
        Workspace.insertClass(n.f1.accept(this, null), newClass);

        return null;
    }

    public String visit(ClassDeclaration n, String argu) throws Exception
    {
        //Create a new class
        ClassN newClass = new ClassN();
        String className = n.f1.accept(this, null);

        //insert class to classTable
        if (Workspace.insertClass(className, newClass) == 0)    //class exists
        {
            throw new Exception("\nLine: " + n.f0.beginLine + " error: " +
                    "duplicate class: " + className);
        }

        if (n.f3.present())
        {
            //add var declarations
            String str = n.f3.accept(this, null);
            String[] declarations = str.split(" ");
            for (int i = 0; i < declarations.length; i += 3)
            {
                //field exists
                if (newClass.insertField(declarations[i + 1], declarations[i]) == 0)
                {
                    errors++;
                    System.out.println("\nLine: " + declarations[i + 2] + " error: variable "
                            + declarations[i + 1] + " is already declared");
                }
            }
        }

        //add method declarations
        n.f4.accept(this, className);

        return null;
    }

    public String visit(ClassExtendsDeclaration n, String argu) throws Exception
    {
        String superClass = n.f3.accept(this, null);
        String subClass = n.f1.accept(this, null);
        ClassN newClass = new ClassN();

        //class exists
        if (Workspace.insertClass(subClass, newClass) == 0)
        {
            throw new Exception("\nLine: " + n.f0.beginLine + " error: " +
                    "duplicate class: " + subClass);
        }


        ClassN superCl = Workspace.getClass(superClass);
        //superclass is not defined
        if (superCl == null)
        {
            errors++;
            System.out.println("\nLine: " + n.f0.beginLine + " error: Undefined superclass "
                    + superClass);
        }
        else
        {
            Workspace.insertSuperClass(subClass, superClass);
        }

        //add fields to new class
        if (n.f5.present())
        {
            String str = n.f5.accept(this, null);
            String[] declarations = str.split(" ");
            for (int i = 0; i < declarations.length; i += 3)
            {
                if (newClass.insertField(declarations[i + 1], declarations[i]) == 0)
                {
                    errors++;
                    System.out.println("\nLine: " + declarations[i + 2] + " error: variable "
                            + declarations[i + 1] + " is already declared");
                }
            }
        }

        //add methods to new class
        n.f6.accept(this, subClass);

        return null;
    }


    public String visit(MethodDeclaration n, String argu) throws Exception
    {
        //Create a new method
        String methodType = n.f1.accept(this, null);
        Method newMethod = new Method(methodType);
        String methodName = n.f2.accept(this, null);
        //get class argu
        ClassN cl = Workspace.getClass(argu);

        String args = "";


        if (n.f4.present())
        {
            //add parameters to new method
            String str = n.f4.accept(this, null);
            String[] parameters = str.split(" ");
            for (int i = 0; i < parameters.length; i += 2)
            {
                args += parameters[i] + " ";
                //parameter exists
                if (newMethod.insertParameter(parameters[i + 1], parameters[i]) == 0)
                {
                    errors++;
                    System.out.println("\nLine: " + n.f0.beginLine + " error: variable "
                            + parameters[i + 1] + " is already declared in method " + methodName);
                }
            }
        }

        //add var declarations to the new method
        if (n.f7.present())
        {
            String str = n.f7.accept(this, null);
            String[] declarations = str.split(" ");
            for (int i = 0; i < declarations.length; i += 3)
            {
                //variable redeclaration
                if (newMethod.insertVariable(declarations[i + 1], declarations[i]) == 0)
                {
                    errors++;
                    System.out.println("\nLine: " + declarations[i + 2] + " error: variable "
                            + declarations[i + 1] + " is already declared in method " + methodName);
                }
            }
        }

        if(cl.getMethod(methodName) != null)
        {
            throw new Exception("\nLine: " + n.f0.beginLine + " error: method " + methodName +
                    " is already defined in class " + argu);
        }

        //find the method in a superclass if exists and return it
        Method m = Workspace.findMethod(argu, methodName);
        if (m != null)
        {
            //subclass' method and superclass' method must have the same return type
            if (!m.getReturnType().equals(methodType))
            {
                errors++;
                System.out.println("\nLine: " + n.f0.beginLine + " error: cannot override " +
                        methodName + ": wrong return type");
            }

            //subclass' method and superclass' method must have the same arguments
            if (!args.equals(m.returnArgs()))
            {
                errors++;
                System.out.println("\nLine: " + n.f0.beginLine + " error: cannot override " +
                        "method  " + methodName + "\nInvalid arguments" +
                        "\nArguments expected: " + m.returnArgs() + "\nMethod's arguments: "
                        + args);
            }
        }

        cl.insertMethod(methodName, newMethod);
        return null;
    }

    public String visit(FormalParameterList n, String argu) throws Exception
    {
        String parameterList = "";
        parameterList += n.f0.accept(this, null) + " ";
        parameterList += n.f1.accept(this, null);
        return parameterList;
    }

    public String visit(FormalParameterTail n, String argu) throws Exception
    {
        return n.f0.accept(this, null);
    }

    public String visit(FormalParameterTerm n, String argu) throws Exception
    {
        return n.f1.accept(this, null);
    }

    public String visit(FormalParameter n, String argu) throws Exception
    {
        return n.f0.accept(this, null) + " " + n.f1.accept(this, null);
    }

    public String visit(NodeListOptional n, String argu) throws Exception
    {
        String result = "";

        for (int i = 0; i < n.size(); i++)
        {
            try
            {
                result += n.elementAt(i).accept(this, argu) + " ";
            }
            catch(Exception e)
            {
                errors++;
                System.out.println(e.getMessage());
            }
        }
        return result;
    }

    public String visit(VarDeclaration n, String argu) throws Exception
    {
        String type = n.f0.accept(this, null);
        String identifier = n.f1.accept(this, null);
        int line = n.f2.beginLine;
        return type + " " + identifier + " " + line;
    }

    public String visit(Identifier n, String argu) throws Exception
    {
        return n.f0.toString();
    }

    public String visit(Type n, String argu) throws Exception
    {
        return n.f0.accept(this, null);
    }

    public String visit(ArrayType n, String argu) throws Exception
    {
        return "int[]";
    }

    public String visit(BooleanType n, String argu) throws Exception
    {
        return "boolean";
    }

    public String visit(IntegerType n, String argu) throws Exception
    {
        return "int";
    }
}