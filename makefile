JAVAC = javac
JVM = java
BIN_DIR = bin
SRC_DIR = src

all: $(BIN_DIR)
	$(MAKE) -C $(SRC_DIR)
	javac -d $(BIN_DIR) -sourcepath $(SRC_DIR) $(SRC_DIR)/Main.java

$(BIN_DIR):
	mkdir -p $(BIN_DIR)
	
clean :
	rm -rf $(BIN_DIR)
